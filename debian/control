Source: fcitx-m17n
Section: utils
Priority: optional
Maintainer: Debian Input Method Team <debian-input-method@lists.debian.org>
Uploaders:
 YunQiang Su <syq@debian.org>,
 Aron Xu <aron@debian.org>,
Build-Depends:
 cmake,
 debhelper-compat (= 13),
 fcitx-bin,
 fcitx-libs-dev (>= 1:4.2.8),
 libm17n-dev,
 pkg-config,
Standards-Version: 4.6.0
Rules-Requires-Root: no
Homepage: https://github.com/fcitx/fcitx-m17n/
Vcs-Git: https://salsa.debian.org/input-method-team/fcitx-m17n.git
Vcs-Browser: https://salsa.debian.org/input-method-team/fcitx-m17n

Package: fcitx-m17n
Architecture: any
Multi-Arch: same
Depends:
 fcitx-modules,
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 fcitx (>= 1:4.2.8),
Description: Free Chinese Input Toy of X - m17n module
 Fcitx is the Free Chinese Input Toy of X, which was initially designed
 for Chinese users, and used XIM protocol. Now it has already evolved
 into a highly modularized, feature rich input method framework for
 Unix-like platforms supporting a considerable amount of frontends,
 backends and modules.
 .
 It is an ideal choice for the vast majority. Many of its features make
 users of Unix-like platforms have a fully modern input experience for
 the first time. It has also greatly lower the threshold for developers,
 making the development of extended functions much easier than ever before.
 .
 This package provides the m17n module, which uses libm17n, a
 multilingual text processing library for the C language.
